//message to be encrypted
const { generateKeyPairSync, publicEncrypt, privateDecrypt } = require('crypto');
var toEncrypt = "Hello World";
var encryptBuffer = Buffer.from(toEncrypt);
var publicKey = '-----BEGIN PUBLIC KEY-----\n'+
'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2AFJlAgBXnXjKhcnFpFH\n'+
'sKcMONltgo8Gw17eUNhwiSw813H1B5PnfLxBpcEt1Etf1STDA6wdpoTHbSsmUu+G\n'+
'zaziTfbr9lR4dM/SyNBB1GLF0yeUR/Zh4/eJ/ZUzQIfZBFpgbtNa+T9Cd3XUNcnP\n'+
'bHQLGw0YFDnrydg9l5n9IL3u8rHtTZzkRTRjmMBE4enKdqtlAnwDRmMRg2sd+ssu\n'+
'UVJ49+E4SpOrxqpohvSTIzVWMnmIlQL8pargYP2Rvd06UwiIk2Qe8VybDAeNePOw\n'+
'QqifK6DjYoKK5yNCvNldz8jV36dwbW1GsMtclRBCVFjf/NLm/hjv3Cl6cV6G4Fxu\n'+
'bQIDAQAB\n'+
'-----END PUBLIC KEY-----';
//encrypt using public key
var encrypted = publicEncrypt(publicKey,encryptBuffer);

console.log(encrypted);

//print out the text and cyphertext
console.log("Text to be encrypted:");
console.log(toEncrypt);
console.log("Result base 64:");
console.log(encrypted.toString('base64'));