var crypto = require('crypto');

var publicKey = '-----BEGIN PUBLIC KEY-----\n'+
'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2AFJlAgBXnXjKhcnFpFH\n'+
'sKcMONltgo8Gw17eUNhwiSw813H1B5PnfLxBpcEt1Etf1STDA6wdpoTHbSsmUu+G\n'+
'zaziTfbr9lR4dM/SyNBB1GLF0yeUR/Zh4/eJ/ZUzQIfZBFpgbtNa+T9Cd3XUNcnP\n'+
'bHQLGw0YFDnrydg9l5n9IL3u8rHtTZzkRTRjmMBE4enKdqtlAnwDRmMRg2sd+ssu\n'+
'UVJ49+E4SpOrxqpohvSTIzVWMnmIlQL8pargYP2Rvd06UwiIk2Qe8VybDAeNePOw\n'+
'QqifK6DjYoKK5yNCvNldz8jV36dwbW1GsMtclRBCVFjf/NLm/hjv3Cl6cV6G4Fxu\n'+
'bQIDAQAB\n'+
'-----END PUBLIC KEY-----';

var sign = 'X9MosyA0zlaVAwPrui4tUQLx/mGy5ndPQOqP7I1lcvPgQz0ZWFjngqK06y2jLX1JhMpGBP4L3K/oOKmZRu67J5ej2tDM5cYeIAfJJMYbDv+exKfD86WuwXYvEXsnBUHngmwrUdlHYbDyGp3oxMFcfAcgRpJot79J/6epaSxyfkA+IBer5z/XdrSMUUAImpyWPB0gxnZmccfMtmV4IY27zdnkA8gCcORjpSgqr3Tcewe41Iy/y01msXFnipv0heulm1o+nAWf65ZSRDaqpzx+vv1+21gut7/VS0ZagsIN3vHU/cjnMsClWLEXxNumylglYuKmnf5YsUI96YCl8XzQkg==';

var verifier = crypto.createVerify('sha256');
verifier.update('hola');
var ver = verifier.verify(publicKey, sign,'base64');

console.log(ver);