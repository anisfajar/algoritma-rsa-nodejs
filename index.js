//encrypt
const crypto = require("crypto")
const data = "my secret data"
const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
	modulusLength: 2048,
})

const encryptedData = crypto.publicEncrypt(
	{
		key: publicKey,
		padding: crypto.constants.RSA_PKCS1_PADDING,
		pkcsHash: "sha256",
	},
	Buffer.from(data)
)
console.log("encypted data: ", encryptedData.toString("base64"))

//decrypt
const decryptedData = crypto.privateDecrypt(
	{
		key: privateKey,
		padding: crypto.constants.RSA_PKCS1_PADDING,
		pkcsHash: "sha256",
	},
	encryptedData
)
console.log("decrypted data: ", decryptedData.toString())


// Create some sample data that we want to sign
const verifiableData = "this need to be verified"

const signature = crypto.sign("sha256", Buffer.from(verifiableData), {
	key: privateKey,
	padding: crypto.constants.RSA_PKCS1_PADDING,
})

console.log(signature.toString("base64"))

const isVerified = crypto.verify(
	"sha256",
	Buffer.from(verifiableData),
	{
		key: publicKey,
		padding: crypto.constants.RSA_PKCS1_PADDING,
	},
	signature
)

// isVerified should be `true` if the signature is valid
console.log("signature verified: ", isVerified)
