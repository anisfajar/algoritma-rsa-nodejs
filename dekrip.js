const { generateKeyPairSync, publicEncrypt, privateDecrypt } = require('crypto');
var encrypted = 'lEPWOrpBwEC4po1kUDbkhSEGe6YmqlbuAtodYaEVMSivH2Uq4nCAc+kIJDp55a9djBDXl5YMF9hRc5DgF0i7GfH/ZFkmX746PUAF/5wXMyhWQoTRakKKEUY7FM1gVWHzM9pboBgcSTFjHCYFlXsg8EPl72jifPzGGHloUaIrLzbyJzRBs0pycQtaJPd73sPZ8R74NsJhtLd83pGB6BcwUV0vQ1fcHjibBTr79mKP80WzDtuqhz294OGorU4wn7JJQIbZxUw7xUphcIT2JcJDmVMqcFnVE5DKAW7sCLyS6Km0jyZb1s0LtQEED4Ebojwb2qoB3E1KxWkSyuqaEAVK3g==';

var privateKey = '-----BEGIN RSA PRIVATE KEY-----\n'+
'MIIEogIBAAKCAQEA2AFJlAgBXnXjKhcnFpFHsKcMONltgo8Gw17eUNhwiSw813H1\n'+
'B5PnfLxBpcEt1Etf1STDA6wdpoTHbSsmUu+GzaziTfbr9lR4dM/SyNBB1GLF0yeU\n'+
'R/Zh4/eJ/ZUzQIfZBFpgbtNa+T9Cd3XUNcnPbHQLGw0YFDnrydg9l5n9IL3u8rHt\n'+
'TZzkRTRjmMBE4enKdqtlAnwDRmMRg2sd+ssuUVJ49+E4SpOrxqpohvSTIzVWMnmI\n'+
'lQL8pargYP2Rvd06UwiIk2Qe8VybDAeNePOwQqifK6DjYoKK5yNCvNldz8jV36dw\n'+
'bW1GsMtclRBCVFjf/NLm/hjv3Cl6cV6G4FxubQIDAQABAoIBACNkQpwAm8jQZAIX\n'+
'fQ4Ti72EAVqnNWw45xDH0IFKlVXEa8uZHl65eypS5ia0ej/YgcE4nsNRRiJsiwh2\n'+
'E3dvS5UTiJjhplPcD0iEbLL+fGrCLEumpoML+YoSmwdzlMwz9fjinf5KnxNLrCZK\n'+
'phSSFmxcrx0ljhZyTxHpXl4imvjsGKRo2krvKBfIZSrQPtyAAsgdSNa63zS0TnWt\n'+
'Tdg/YlwkBmreIKGJTdrAvFL24OxwzBtgc+Lc7UoLma4toxLaXMdj/SILZa2s0P5s\n'+
'wnIUKxN1jNIut1XbaXimDJ/HGkaP9lCgcTVLYnC2RS3u2Zvmg97lK0vst26uXltH\n'+
'tI3JiPkCgYEA/FTE1Q5PI9jEbGEYz/bf9IjbVIjhJbrkGRvNHiIWciZtVQTsoU9p\n'+
'SqBnNXsq1TID2W8uhpX/7WemHWnY84randL25sztiZHLUR8Yb7sbWC4ap5OvHG5L\n'+
'RbVAkWrtN1LlPhj6kmJUB+VfIarWX9HMLUdkRoRx4JcdtKUomw8cy98CgYEA2yVO\n'+
'D7ep/XsQV6XDjHRvRULsB8v1CC6Tu0cm50D0kMn/cnVl5GAn3e38FMaS9YHE54RK\n'+
'+knBr2FIC9PT3SyiZkrvDPYt7I55Vj7O9VXu6toI7hyCT2dZF5w50svZgXiv+yEn\n'+
'77EQAGCyCbQDD/rYnKV7WmfRzBnybeHPB3wKTzMCgYBKMob6e02sJcaEc/y+0kJC\n'+
'fqe38NP7qzv9Yh5rmOxuvnvxvxPxzw8QAaC3qjMLI7zcCARvUNZDsfG3vnMsEg18\n'+
'0RcHAQVsKXHWtcUSvbspr8zor9WTD5+UrNqCqCV+2iJMoz2zI7S/6aOSm9CTtnZK\n'+
'bi6/SZzOxMDYaKRLDCOUkwKBgFDf0ZEnXMq5MezGtgYNqkrLScQO1xzv44FH3umV\n'+
'JSyR/33EKZhAjDXw8AjSx8Vm4L34Kpt3wMzyf94tjJChOkW5/cKwz33vjoPvpf0Q\n'+
'e52cXZNSXhzjrRNzijtrE5gebAOn1s80bw1Uoh/GmSK6llopD/7rNZe3M6qtnPPM\n'+
'KRnVAoGACFl4SFZ9lBMvaJraBaOQva4F0HztDeRpdfRry88sDXAsxtraEMpJfz3j\n'+
'QlGPZtvsldk0fNRyUyAmjnd5K2CMHvBZoqs9UvJhjtymJti2sEmL+fTF0gM1JXc8\n'+
'KbxRSi1d3jYiFIKUY1Pjopa5jWzYm14tTwjcAjS96QVLeXcdkP0=\n'+
'-----END RSA PRIVATE KEY-----';

//decrypt the cyphertext using the private key
var decryptBuffer = Buffer.from(encrypted.toString("base64"), "base64");
var decrypted = privateDecrypt(privateKey,decryptBuffer);

//print out the decrypted text
console.log("decripted Text:");
console.log(decrypted.toString());