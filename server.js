const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 5593;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/helloworld', (req, res) => {
  res.send('Hello World!')
});
require("./app/routes/algoritma_rsa")(app);

/* const crypto = require('crypto'); 
const { generateKeyPairSync } = require('crypto'); 
const { publicKey, privateKey } = generateKeyPairSync('rsa', 
{   modulusLength: 2048,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem'   
    },   
    privateKeyEncoding: {
      type: 'pkcs1',
      format: 'pem',
      //cipher: 'aes-256-cbc'
      //passphrase: 'top secret'   
  } 
}); 

app.post('/generatersa', (req, res) => {
    res.send({
        private:privateKey,
        public:publicKey,
    });
})
app.post('/enkripsi', (req, res) => {
    res.send({
        body:req.body
    })
}) */

app.listen(port, () => {
  console.log(`cli-nodejs-api listening at http://localhost:${port}`)
});