module.exports = app => { 
    const approutes = require("../controllers/algoritma_rsa.js");
    var router = require("express").Router();
    router.post("/generate", approutes.generate);
    router.post("/enkripsi", approutes.enkrip);
    router.post("/dekripsi", approutes.dekrip);
    router.post("/sign", approutes.signed);
    router.post("/verify", approutes.verifikasi);
    app.use('/api/rsa', router);
}