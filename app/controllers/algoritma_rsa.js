const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const crypto = require('crypto'); 
const { publicEncrypt, privateDecrypt } = require('crypto');
const { generateKeyPairSync } = require('crypto'); 
const { sign } = require('eccrypto');
const { publicKey, privateKey } = generateKeyPairSync('rsa', 
{   modulusLength: 2048,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem'   
    },   
    privateKeyEncoding: {
      type: 'pkcs1',
      format: 'pem',
      //cipher: 'aes-256-cbc'
      //passphrase: 'top secret'   
  } 
}); 
exports.generate = (req, res) => {
  res.send({
      private:privateKey,
      public:publicKey,
  });
}

exports.enkrip = (req, res) => {
  if(!req.body.plain){
    res.status(400).send({
        message: "plain tidak boleh kosong!",
        status: 400
    });
    return;
  } else {
   
    var toEncrypt = req.body.plain;
    var encryptBuffer = Buffer.from(toEncrypt);
    var publicKey = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs/AZDpw5SfIK9Lamqvkt\nIUJ4rixJE3exdtwlx/S1VMnF/elwEXK0xgc7cNoKGl6zGDYvp1+e9Az8GZp2zH+v\nox0p+qJ84kwZ3TUO4rMPXmbL6QewBS3CpmOwHmrb450TB/Qur8oxSbBiY7I0zIh2\nSbdMDcezTfkPYIkijfVSojS8KgzhB55yaLmvYacqrSFY6m1g6+xsWtaXihrs1Qxd\nvDpKT/UQnRDalCQ7Ym4rod1/bdMnh4/4jONaXdLlIID4Mqrj8qcJwWb1k9dmxrbp\neGhWsRFT7Agr4DeAehsd1MuxzTAZxZceeBbeyeGmnURvdGPRmZTUGFcpxjVMdKLs\nfQIDAQAB\n-----END PUBLIC KEY-----\n';
    //encrypt using public key
    var encrypted = publicEncrypt(publicKey,encryptBuffer);
    res.send({
      enkrip:encrypted.toString('base64')
    })
  }
}

exports.dekrip = (req, res) => {
  if(!req.body.encode){
    res.status(400).send({
        message: "encode tidak boleh kosong!",
        status: 400
    });
    return;
  } else {
    var encrypted = req.body.encode

    var privateKey = '-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEAs/AZDpw5SfIK9LamqvktIUJ4rixJE3exdtwlx/S1VMnF/elw\nEXK0xgc7cNoKGl6zGDYvp1+e9Az8GZp2zH+vox0p+qJ84kwZ3TUO4rMPXmbL6Qew\nBS3CpmOwHmrb450TB/Qur8oxSbBiY7I0zIh2SbdMDcezTfkPYIkijfVSojS8Kgzh\nB55yaLmvYacqrSFY6m1g6+xsWtaXihrs1QxdvDpKT/UQnRDalCQ7Ym4rod1/bdMn\nh4/4jONaXdLlIID4Mqrj8qcJwWb1k9dmxrbpeGhWsRFT7Agr4DeAehsd1MuxzTAZ\nxZceeBbeyeGmnURvdGPRmZTUGFcpxjVMdKLsfQIDAQABAoIBAQCecri94teXw+nd\n39EYEV+qQAxg42JeIDJHhkhGsbfTt/H/S0aCNs30631uPLJU0jtHAMAp1Mv2AFEm\ngpxGK9IQAqhqqhbqdzT3U7eN7TuFV2QMCdb4WHllZ7c4scR7xF9zy5Qo/BU23Hln\nbQ+NZEU1WJFCOS4tZTojOd8RvotLhUXFiANBc8oobGvIMPRPpUALS7+wZ8/Hr5Gc\nH9YPAeNWnRT4z7ZJFF5QPo8nHDA0RKcX6Jf82D2j/yV/LbgCtEz2gEvTMFM3ou2g\n2ViTvA0KZpV7BMtXIYC+8S2nC8FB3yXhfuiZ1ToWCpu3UrRoJrmO98J6UdHwRCvV\nrVkAaMqBAoGBAOSei27bnzO4D8wwEg587ilChZlpdEBUUqLipp+BeCMwC4gpQCay\n7oCxzialny1Y9mni4ssricSm0AZw85Sx9t8pj8NWunJl/GSTMdmW9Rg6ZTmJoj1v\nl1zNTHgvPohDDJ3lY+cGSoPxeR0sGnQUBQnknr/U6pm7PWBEbiEQ6YmRAoGBAMl8\n+7TvhW9OsnBqI0oo+KQ9XPSj+yFEGThto2cEEaHS3ROEK2uZ5vuU3NAkIBO56yjB\nwpK4amjBuUDSkgsK3Ju+DXaQEhLK6xIpSyqq7G0F0aT59shcLbO4P0TVZlQoep0y\nKuxvDaGC5VPO/SqptH+zIKXpYDDBRfyZ5UTcqt4tAoGBALyzmIdCQ9/elzuEmgLX\nX4Uy4S9Feaz3bNYBmmKh7NWufHZBBjBrFK2pFW2U2xQFEC9KFMR6BVcKET6f0I31\nbr24gm9LkM2IHJGvZUAid98tpAuf7QgkqZzx36J77HNwVGOnyZQQ/0R09fD8TEFv\nnNEToWzZg2Ksx/ZDxVsJqJnhAoGBAJSyhlhbxFAG9i18Rxr97bHYD3gEZNuCFFLO\npx7po9GXesxiaGD6rbrRe7yoTHwCY4gqptv8+ZriDUPHozw/Adx2gB0V6iCwXJeE\nnhKZHe6/sp5cU0I+tQPDX/3+t4w4a3UDhyYCNz8RCQm24qPPpoPvwJJxDyosGOh4\n8vjFGdzpAoGAHT/e5pY40FvLZYTeXh7HPfVf2tQFO1AVK7KASNM3gnl9/saZmCJx\nHXxzewoHZTEio0JHZcHVI+HIYbJ816DejteQ0hgSTevmW7/kXrAzwXTMhYKVjN/O\nFgAskUJA0ahjEVSBnnevKWmERRYwf5gy0hPeuUwJ+JQ4y37Teul9Fdg=\n-----END RSA PRIVATE KEY-----\n';

    var decryptBuffer = Buffer.from(encrypted.toString("base64"), "base64");
    var decrypted = privateDecrypt(privateKey,decryptBuffer);
    res.send({
      dekrip:decrypted.toString()
    })
  }
}

exports.signed = (req, res) => {
  if(!req.body.plain){
    res.status(400).send({
        message: "plain tidak boleh kosong!",
        status: 400
    });
    return;
  } else {
    var encrypted = req.body.plain

    var privateKey = '-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEAs/AZDpw5SfIK9LamqvktIUJ4rixJE3exdtwlx/S1VMnF/elw\nEXK0xgc7cNoKGl6zGDYvp1+e9Az8GZp2zH+vox0p+qJ84kwZ3TUO4rMPXmbL6Qew\nBS3CpmOwHmrb450TB/Qur8oxSbBiY7I0zIh2SbdMDcezTfkPYIkijfVSojS8Kgzh\nB55yaLmvYacqrSFY6m1g6+xsWtaXihrs1QxdvDpKT/UQnRDalCQ7Ym4rod1/bdMn\nh4/4jONaXdLlIID4Mqrj8qcJwWb1k9dmxrbpeGhWsRFT7Agr4DeAehsd1MuxzTAZ\nxZceeBbeyeGmnURvdGPRmZTUGFcpxjVMdKLsfQIDAQABAoIBAQCecri94teXw+nd\n39EYEV+qQAxg42JeIDJHhkhGsbfTt/H/S0aCNs30631uPLJU0jtHAMAp1Mv2AFEm\ngpxGK9IQAqhqqhbqdzT3U7eN7TuFV2QMCdb4WHllZ7c4scR7xF9zy5Qo/BU23Hln\nbQ+NZEU1WJFCOS4tZTojOd8RvotLhUXFiANBc8oobGvIMPRPpUALS7+wZ8/Hr5Gc\nH9YPAeNWnRT4z7ZJFF5QPo8nHDA0RKcX6Jf82D2j/yV/LbgCtEz2gEvTMFM3ou2g\n2ViTvA0KZpV7BMtXIYC+8S2nC8FB3yXhfuiZ1ToWCpu3UrRoJrmO98J6UdHwRCvV\nrVkAaMqBAoGBAOSei27bnzO4D8wwEg587ilChZlpdEBUUqLipp+BeCMwC4gpQCay\n7oCxzialny1Y9mni4ssricSm0AZw85Sx9t8pj8NWunJl/GSTMdmW9Rg6ZTmJoj1v\nl1zNTHgvPohDDJ3lY+cGSoPxeR0sGnQUBQnknr/U6pm7PWBEbiEQ6YmRAoGBAMl8\n+7TvhW9OsnBqI0oo+KQ9XPSj+yFEGThto2cEEaHS3ROEK2uZ5vuU3NAkIBO56yjB\nwpK4amjBuUDSkgsK3Ju+DXaQEhLK6xIpSyqq7G0F0aT59shcLbO4P0TVZlQoep0y\nKuxvDaGC5VPO/SqptH+zIKXpYDDBRfyZ5UTcqt4tAoGBALyzmIdCQ9/elzuEmgLX\nX4Uy4S9Feaz3bNYBmmKh7NWufHZBBjBrFK2pFW2U2xQFEC9KFMR6BVcKET6f0I31\nbr24gm9LkM2IHJGvZUAid98tpAuf7QgkqZzx36J77HNwVGOnyZQQ/0R09fD8TEFv\nnNEToWzZg2Ksx/ZDxVsJqJnhAoGBAJSyhlhbxFAG9i18Rxr97bHYD3gEZNuCFFLO\npx7po9GXesxiaGD6rbrRe7yoTHwCY4gqptv8+ZriDUPHozw/Adx2gB0V6iCwXJeE\nnhKZHe6/sp5cU0I+tQPDX/3+t4w4a3UDhyYCNz8RCQm24qPPpoPvwJJxDyosGOh4\n8vjFGdzpAoGAHT/e5pY40FvLZYTeXh7HPfVf2tQFO1AVK7KASNM3gnl9/saZmCJx\nHXxzewoHZTEio0JHZcHVI+HIYbJ816DejteQ0hgSTevmW7/kXrAzwXTMhYKVjN/O\nFgAskUJA0ahjEVSBnnevKWmERRYwf5gy0hPeuUwJ+JQ4y37Teul9Fdg=\n-----END RSA PRIVATE KEY-----\n';

    var signer = crypto.createSign('sha256')
    signer.update(encrypted);
    var sign = signer.sign(privateKey,'base64')

    if(!req.body.plain){
      res.status(400).send({
        message: "plain tidak boleh kosong!",
        status: 400
      });
      return;
    } else {
      res.send({
        datasign:sign
      })
    }
  }
}

exports.verifikasi = (req, res) => {
  if(!req.body.sign){
    res.status(400).send({
        message: "sign tidak boleh kosong!",
        status: 400
    });
    return;
  } else {
   
    var publicKey = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs/AZDpw5SfIK9Lamqvkt\nIUJ4rixJE3exdtwlx/S1VMnF/elwEXK0xgc7cNoKGl6zGDYvp1+e9Az8GZp2zH+v\nox0p+qJ84kwZ3TUO4rMPXmbL6QewBS3CpmOwHmrb450TB/Qur8oxSbBiY7I0zIh2\nSbdMDcezTfkPYIkijfVSojS8KgzhB55yaLmvYacqrSFY6m1g6+xsWtaXihrs1Qxd\nvDpKT/UQnRDalCQ7Ym4rod1/bdMnh4/4jONaXdLlIID4Mqrj8qcJwWb1k9dmxrbp\neGhWsRFT7Agr4DeAehsd1MuxzTAZxZceeBbeyeGmnURvdGPRmZTUGFcpxjVMdKLs\nfQIDAQAB\n-----END PUBLIC KEY-----\n';
    var valsign = req.body.sign;
    var plain = req.body.plain

    if(!req.body.plain) {
      res.status(400).send({
        message: "plain tidak boleh kosong!",
        status: 400
      });
      return;
    } else {
      var verifier = crypto.createVerify('sha256');
      verifier.update(plain);
      var ver = verifier.verify(publicKey, valsign,'base64');
      if(ver == true){
        res.send({
          status:"Valid Signature"
        })
      } else {
        res.send({
          status:"Invalid Signature"
        })
      }
    }
 
  }
}