var eccrypto = require("eccrypto");

var privateKeyA = eccrypto.generatePrivate();
var publicKeyA = eccrypto.getPublic(privateKeyA);
var privateKeyB = eccrypto.generatePrivate();
var publicKeyB = eccrypto.getPublic(privateKeyB);
var plain = "hello world";

var encryptBuffer = Buffer.from(plain)
var encryptedbufferbase64 = encryptBuffer.toString('base64')

// Encrypting
eccrypto.encrypt(publicKeyB, encryptedbufferbase64).then(function(encrypted) {
  console.log("Encrypt: "+encryptedbufferbase64);
  // decrypting
  eccrypto.decrypt(privateKeyB, encrypted).then(function() {
    console.log("Decrypt : "+Buffer.from(encryptedbufferbase64, 'base64').toString('ascii'))
  });
});